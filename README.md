# READ ME #

### 1.	Spark program for processing log files stored on S3 and store aggregated data on different S3 bucket ###
* ELB Access logs
* CloudFront access logs
* S3 Access logs

### 2.	Generate following output from the respective logs ###
 a. CloudFront Logs

* Top 10 popular objects
* Total amount of data downloaded
* Total distinct object downloaded.
* Top 10 IP repo Client IP
* Data server from each Edge Location

 b. ELB Logs

* Total request for each response code
* Total error response in a day

 c. S3 Logs

* Top 10 popular objects
* Total amount of data downloaded
* Total distinct object downloaded.

# How to Run #

## Local Mode ##

### Prerequisites ### 
* spark prebuilt with hadoop zip file & java 1.7
* For Spark - Download to home directory & unzip from location : https://d3kbcqa49mib13.cloudfront.net/spark-1.6.3-bin-hadoop2.3.tgz

### Commands ###
* cd astro
* mvn clean install
* cp target/astro/spark-executable.jar ~/

* cd ~/spark-1.6.3-bin-hadoop2.3
* For Problem 1 : 
./bin/spark-submit --class com.astro.task.AccessLogAggregationTask --master local ~/spark-executable.jar file:////../all_logs/* file:////../output

* For Problem 2a : 
./bin/spark-submit --class com.astro.task.CloudfrontAccessLogAnalysisTask --master local ~/spark-executable.jar file:////../cloudfront-sample-logs.txt ../cf_output.txt

* For Problem 2b :
./bin/spark-submit --class com.astro.task.ElbAccessLogAnalysisTask --master local ~/spark-executable.jar file:////../elb-access-logs.txt ../elb_output.txt

* For Problem 2c
./bin/spark-submit --class com.astro.task.ServerAccessLogAnalysisTask --master local ~/spark-executable.jar file:////../server-access-logs.txt ../server_output.txt