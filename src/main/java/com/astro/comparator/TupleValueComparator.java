package com.astro.comparator;

import scala.Tuple2;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.Comparator;

/**
 * Custom comparator for Tuple2 value
 */
public class TupleValueComparator<K,V> implements Comparator<Tuple2<K, V>>, Serializable {

	@Override
	public int compare(Tuple2<K, V> o1, Tuple2<K, V> o2) {
		Class<?> aClass = o1._2().getClass();
		try {
			Method compareTo = aClass.getDeclaredMethod("compareTo", aClass);
			return (int) compareTo.invoke(o1._2(), o2._2());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
