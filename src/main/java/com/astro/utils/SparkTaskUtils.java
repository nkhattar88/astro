package com.astro.utils;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * General Utils class for for all Spark Tasks
 */
public class SparkTaskUtils {

	private static final Logger LOGGER = Logger.getLogger(SparkTaskUtils.class);

	public static final String HTTP_METHOD_GET = "GET";
	public static final String HTTP_STATUS_200 = "200";

	/**
	 * Function definition to filter empty input
	 */
	public static Function<String, Boolean> filterEmptyFn = new Function<String, Boolean>() {
		@Override
		public Boolean call(String s) throws Exception {
			return StringUtils.isNotBlank(s);
		}
	};

	/**
	 * Function definition to reduce sum of numeric input
	 */
	public static Function2<Long, Long, Long> _reduceNumericSumFn = new Function2<Long, Long, Long>() {
		@Override
		public Long call(Long v1, Long v2) throws Exception {
			return v1 + v2;
		}
	};

	public static void writeLogAnalysisOutputToFile(String ouputText, String analysisOutputPath) throws IOException {
		File outputFile = new File(analysisOutputPath);
		if(outputFile.exists()) {
			outputFile.delete();
		}
		outputFile.createNewFile();
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter(outputFile));
			writer.write("\n");
			writer.write(ouputText);
			writer.write("\n");
		} catch (IOException e) {
			LOGGER.error("Error in writing analysis output to file:"+analysisOutputPath, e);
			throw e;
		}
		finally {
			if(writer != null) {
				writer.flush();
				writer.close();
			}
		}
	}
}
