package com.astro.utils;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *  Utils class for Parsing Input Text to target object
 */
public class ParseUtils{

	private static final Logger LOGGER = Logger.getLogger(ParseUtils.class);

	/**
	 * Method to map parse plain text & map values to fields of Target object
	 * @param logText Input text to parse
	 * @param target  Target Object to populate
	 * @param regexPattern Regex pattern to use for parsing
	 */
	public static void mapLogTextToTargetObjectFields(String logText, Object target, String regexPattern) {
		if(StringUtils.isNotBlank(logText)) {
			List<String> matchList = parseToList(logText, regexPattern);
			if(!matchList.isEmpty()) {
				int i = 0;
				for (Field field : target.getClass().getDeclaredFields()) {
					if(i == matchList.size()) {
						break;
					}
					String value = matchList.get(i++);
					if(StringUtils.isNotBlank(value)
							&& !"null".equalsIgnoreCase(value)
							&& !"-".equalsIgnoreCase(value)) {
						Method setter;
						try {
							setter = target.getClass().getDeclaredMethod("set" + StringUtils.capitalize(field.getName()), String.class);
							setter.invoke(target, value);
						} catch (Exception e) {
							//ignore error & proceed
							LOGGER.error("Unable to set value for field:"+field.getName());
						}
					}
				}
			}
		}
	}

	private static List<String> parseToList(String logText, String regexPattern) {
		List<String> matchList = new ArrayList<>();
		Pattern regex = Pattern.compile(regexPattern);
		Matcher regexMatcher = regex.matcher(logText);
		while (regexMatcher.find()) {
			matchList.add(regexMatcher.group().replaceAll("\"|'",""));
		}
		return matchList;
	}
}
