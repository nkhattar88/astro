package com.astro.dto;

import scala.Tuple2;

import java.io.Serializable;
import java.util.List;

public class ServerAccessLogAnalysisOutput implements Serializable {
	private List<Tuple2<String, Long>> topPopularObjects;
	private Long totalDownloadedData;
	private Long totalDistinctObjectDownloaded;

	public void setTopPopularObjects(List<Tuple2<String, Long>> topPopularObjects) {
		this.topPopularObjects = topPopularObjects;
	}

	public List<Tuple2<String, Long>> getTopPopularObjects() {
		return topPopularObjects;
	}

	public void setTotalDownloadedData(Long totalDownloadedData) {
		this.totalDownloadedData = totalDownloadedData;
	}

	public Long getTotalDownloadedData() {
		return totalDownloadedData;
	}

	public void setTotalDistinctObjectDownloaded(Long totalDistinctObjectDownloaded) {
		this.totalDistinctObjectDownloaded = totalDistinctObjectDownloaded;
	}

	public Long getTotalDistinctObjectDownloaded() {
		return totalDistinctObjectDownloaded;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append("topPopularObjects : ").append(topPopularObjects).append("\n");
		sb.append("totalDownloadedData : ").append(totalDownloadedData).append("\n");
		sb.append("totalDistinctObjectDownloaded : ").append(totalDistinctObjectDownloaded).append("\n");
		return sb.toString();
	}
}
