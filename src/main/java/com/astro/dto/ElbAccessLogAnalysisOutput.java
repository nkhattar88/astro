package com.astro.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

public class ElbAccessLogAnalysisOutput implements Serializable {
	private Map<String, Long> totalRequestByCode;
	private Map<Date, Long> totalErrorPerDay;

	public void setTotalRequestByCode(Map<String, Long> totalRequestByCode) {
		this.totalRequestByCode = totalRequestByCode;
	}

	public Map<String, Long> getTotalRequestByCode() {
		return totalRequestByCode;
	}

	public void setTotalErrorPerDay(Map<Date, Long> totalErrorPerDay) {
		this.totalErrorPerDay = totalErrorPerDay;
	}

	public Map<Date, Long> getTotalErrorPerDay() {
		return totalErrorPerDay;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append("totalRequestByCode : ").append(totalRequestByCode).append("\n");
		sb.append("totalErrorPerDay : ").append(totalErrorPerDay).append("\n");
		return sb.toString();
	}
}
