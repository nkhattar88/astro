package com.astro.dto;

import scala.Tuple2;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class CfAccessLogAnalysisOutput  implements Serializable {
	private List<Tuple2<String, Long>> topPopularObjects;
	private Long totalDownloadedData;
	private Long totalDistinctObjectDownloaded;
	private List<Tuple2<String, Long>> topClientIPs;
	private Map<String, Long> totalDataServerPerEdgeLocation;

	public void setTopPopularObjects(List<Tuple2<String, Long>> topPopularObjects) {
		this.topPopularObjects = topPopularObjects;
	}

	public List<Tuple2<String, Long>> getTopPopularObjects() {
		return topPopularObjects;
	}

	public void setTotalDownloadedData(Long totalDownloadedData) {
		this.totalDownloadedData = totalDownloadedData;
	}

	public Long getTotalDownloadedData() {
		return totalDownloadedData;
	}

	public void setTotalDistinctObjectDownloaded(Long totalDistinctObjectDownloaded) {
		this.totalDistinctObjectDownloaded = totalDistinctObjectDownloaded;
	}

	public Long getTotalDistinctObjectDownloaded() {
		return totalDistinctObjectDownloaded;
	}

	public void setTopClientIPs(List<Tuple2<String, Long>> topClientIPs) {
		this.topClientIPs = topClientIPs;
	}

	public List<Tuple2<String, Long>> getTopClientIPs() {
		return topClientIPs;
	}

	public void setTotalDataServedPerEdgeLocation(Map<String, Long> totalDataServerPerEdgeLocation) {
		this.totalDataServerPerEdgeLocation = totalDataServerPerEdgeLocation;
	}

	public Map<String, Long> getTotalDataServerPerEdgeLocation() {
		return totalDataServerPerEdgeLocation;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append("topPopularObjects : ").append(topPopularObjects).append("\n");
		sb.append("totalDownloadedData : ").append(totalDownloadedData).append("\n");
		sb.append("totalDistinctObjectDownloaded : ").append(totalDistinctObjectDownloaded).append("\n");
		sb.append("topClientIPs : ").append(topClientIPs).append("\n");
		sb.append("totalDataServerPerEdgeLocation : ").append(totalDataServerPerEdgeLocation).append("\n");
		return sb.toString();
	}
}
