package com.astro.dto;

import com.astro.utils.ParseUtils;

import java.io.Serializable;

public class ServerAccessLogInput implements Serializable {

	private String bucketOwner;
	private String bucketName;
	private String timestamp;
	private String remoteIP;
	private String requester;
	private String requestId;
	private String operation;
	private String key;
	private String requestUri;
	private String httpStatus;
	private String errorCode;
	private String bytesSent;
	private String objSize;
	private String totalTime;
	private String turnAroundTime;
	private String referrer;
	private String userAgent;
	private String versionId;

	public String getBucketOwner() {
		return bucketOwner;
	}

	public void setBucketOwner(String bucketOwner) {
		this.bucketOwner = bucketOwner;
	}

	public String getBucketName() {
		return bucketName;
	}

	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getRemoteIP() {
		return remoteIP;
	}

	public void setRemoteIP(String remoteIP) {
		this.remoteIP = remoteIP;
	}

	public String getRequester() {
		return requester;
	}

	public void setRequester(String requester) {
		this.requester = requester;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getRequestUri() {
		return requestUri;
	}

	public void setRequestUri(String requestUri) {
		this.requestUri = requestUri;
	}

	public String getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(String httpStatus) {
		this.httpStatus = httpStatus;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getBytesSent() {
		return bytesSent;
	}

	public void setBytesSent(String bytesSent) {
		this.bytesSent = bytesSent;
	}

	public String getObjSize() {
		return objSize;
	}

	public void setObjSize(String objSize) {
		this.objSize = objSize;
	}

	public String getTotalTime() {
		return totalTime;
	}

	public void setTotalTime(String totalTime) {
		this.totalTime = totalTime;
	}

	public String getTurnAroundTime() {
		return turnAroundTime;
	}

	public void setTurnAroundTime(String turnAroundTime) {
		this.turnAroundTime = turnAroundTime;
	}

	public String getReferrer() {
		return referrer;
	}

	public void setReferrer(String referrer) {
		this.referrer = referrer;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public String getVersionId() {
		return versionId;
	}

	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("com.astro.dto.ServerAccessLogInput{");
		sb.append("bucketOwner='").append(bucketOwner).append('\'');
		sb.append(", bucketName='").append(bucketName).append('\'');
		sb.append(", timestamp='").append(timestamp).append('\'');
		sb.append(", remoteIP='").append(remoteIP).append('\'');
		sb.append(", requester='").append(requester).append('\'');
		sb.append(", requestId='").append(requestId).append('\'');
		sb.append(", operation='").append(operation).append('\'');
		sb.append(", key='").append(key).append('\'');
		sb.append(", requestUri='").append(requestUri).append('\'');
		sb.append(", httpStatus='").append(httpStatus).append('\'');
		sb.append(", errorCode='").append(errorCode).append('\'');
		sb.append(", bytesSent='").append(bytesSent).append('\'');
		sb.append(", objSize='").append(objSize).append('\'');
		sb.append(", totalTime='").append(totalTime).append('\'');
		sb.append(", turnAroundTime='").append(turnAroundTime).append('\'');
		sb.append(", referrer='").append(referrer).append('\'');
		sb.append(", userAgent='").append(userAgent).append('\'');
		sb.append(", versionId='").append(versionId).append('\'');
		sb.append('}');
		return sb.toString();
	}

	public ServerAccessLogInput map(String logText) {
		//split using spaces except enclosed in double quotes or singe quotes or enclosing brackets
		String regexPattern = "[^\\s\"'\\[]+|\"[^\"]*\"|'[^']*'|\\[[^\\]]*\\]";
		ParseUtils.mapLogTextToTargetObjectFields(logText, this, regexPattern);
		return this;
	}

	public static void main(String[] args) {
		System.out.println(new ServerAccessLogInput().map("79a59df900b949e55d96a1e698fbacedfd6e09d98eacf8f8d5218e7cd47ef2be mybucket [06/Feb/2014:00:01:57 +0000] 192.0.2.3 79a59df900b949e55d96a1e698fbacedfd6e09d98eacf8f8d5218e7cd47ef2be DD6CC733AEXAMPLE REST.PUT.OBJECT s3-dg.pdf \"PUT /mybucket/s3-dg.pdf HTTP/1.1\" 200 - - 4406583 41754 28 \"-\" \"S3Console/0.4\" -"));
	}
}
