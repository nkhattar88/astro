package com.astro.dto;

import com.astro.utils.ParseUtils;

import java.io.Serializable;

public class CloudfrontAccessLogInput implements Serializable {

	private String date;
	private String time;
	private String edgeLocation;
	private String scBytes;
	private String clientIP;
	private String csMethod;
	private String csHost;
	private String csUriStem;
	private String scStatus;
	private String csReferrer;
	private String csUserAgent;
	private String csUriQuery;
	private String csCookie;
	private String edgeResultType;
	private String edgeRequestId;
	private String hostHeader;
	private String csProtocol;
	private String csBytes;
	private String timeTaken;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getEdgeLocation() {
		return edgeLocation;
	}

	public void setEdgeLocation(String edgeLocation) {
		this.edgeLocation = edgeLocation;
	}

	public String getScBytes() {
		return scBytes;
	}

	public void setScBytes(String scBytes) {
		this.scBytes = scBytes;
	}

	public String getClientIP() {
		return clientIP;
	}

	public void setClientIP(String clientIP) {
		this.clientIP = clientIP;
	}

	public String getCsMethod() {
		return csMethod;
	}

	public void setCsMethod(String csMethod) {
		this.csMethod = csMethod;
	}

	public String getCsHost() {
		return csHost;
	}

	public void setCsHost(String csHost) {
		this.csHost = csHost;
	}

	public String getCsUriStem() {
		return csUriStem;
	}

	public void setCsUriStem(String csUriStem) {
		this.csUriStem = csUriStem;
	}

	public String getScStatus() {
		return scStatus;
	}

	public void setScStatus(String scStatus) {
		this.scStatus = scStatus;
	}

	public String getCsReferrer() {
		return csReferrer;
	}

	public void setCsReferrer(String csReferrer) {
		this.csReferrer = csReferrer;
	}

	public String getCsUserAgent() {
		return csUserAgent;
	}

	public void setCsUserAgent(String csUserAgent) {
		this.csUserAgent = csUserAgent;
	}

	public String getCsUriQuery() {
		return csUriQuery;
	}

	public void setCsUriQuery(String csUriQuery) {
		this.csUriQuery = csUriQuery;
	}

	public String getCsCookie() {
		return csCookie;
	}

	public void setCsCookie(String csCookie) {
		this.csCookie = csCookie;
	}

	public String getEdgeResultType() {
		return edgeResultType;
	}

	public void setEdgeResultType(String edgeResultType) {
		this.edgeResultType = edgeResultType;
	}

	public String getEdgeRequestId() {
		return edgeRequestId;
	}

	public void setEdgeRequestId(String edgeRequestId) {
		this.edgeRequestId = edgeRequestId;
	}

	public String getHostHeader() {
		return hostHeader;
	}

	public void setHostHeader(String hostHeader) {
		this.hostHeader = hostHeader;
	}

	public String getCsProtocol() {
		return csProtocol;
	}

	public void setCsProtocol(String csProtocol) {
		this.csProtocol = csProtocol;
	}

	public String getCsBytes() {
		return csBytes;
	}

	public void setCsBytes(String csBytes) {
		this.csBytes = csBytes;
	}

	public String getTimeTaken() {
		return timeTaken;
	}

	public void setTimeTaken(String timeTaken) {
		this.timeTaken = timeTaken;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("com.astro.dto.CloudfrontAccessLogInput{");
		sb.append("date='").append(date).append('\'');
		sb.append(", time='").append(time).append('\'');
		sb.append(", edgeLocation='").append(edgeLocation).append('\'');
		sb.append(", scBytes='").append(scBytes).append('\'');
		sb.append(", clientIP='").append(clientIP).append('\'');
		sb.append(", csMethod='").append(csMethod).append('\'');
		sb.append(", csHost='").append(csHost).append('\'');
		sb.append(", csUriStem='").append(csUriStem).append('\'');
		sb.append(", scStatus='").append(scStatus).append('\'');
		sb.append(", csReferrer='").append(csReferrer).append('\'');
		sb.append(", csUserAgent='").append(csUserAgent).append('\'');
		sb.append(", csUriQuery='").append(csUriQuery).append('\'');
		sb.append(", csCookie='").append(csCookie).append('\'');
		sb.append(", edgeResultType='").append(edgeResultType).append('\'');
		sb.append(", edgeRequestId='").append(edgeRequestId).append('\'');
		sb.append(", hostHeader='").append(hostHeader).append('\'');
		sb.append(", csProtocol='").append(csProtocol).append('\'');
		sb.append(", csBytes='").append(csBytes).append('\'');
		sb.append(", timeTaken='").append(timeTaken).append('\'');
		sb.append('}');
		return sb.toString();
	}

	public CloudfrontAccessLogInput map(String logText) {
		//split using spaces except enclosed in double quotes or singe quotes or enclosing brackets
		String regexPattern = "[^\\t\"'\\[]+|\"[^\"]*\"|'[^']*'|\\[[^\\]]*\\]";
		ParseUtils.mapLogTextToTargetObjectFields(logText, this, regexPattern);
		return this;
	}

	public static void main(String[] args) {
		System.out.println(new CloudfrontAccessLogInput().map("2015-05-12\t\"17:07:44\"\tHKG51\t1163801\t175.144.252.199\tGET\ttest.cloudfront.net\t/vod/PACKHBBNXP0010141262/movie/1/HBBNX01HM01/Layer2/HBBNX01HM01Layer2_00050.ts\t200\t-\tAppleCoreMedia/1.0.0.12F69%2520(iPad;%2520U;%2520CPU%2520OS%25208_3%2520like%2520Mac%2520OS%2520X;%2520en_us)\t-\t-\tHit\tUdB-UxSnGKKlkx2sF8B30x6awGTAXLMlr1q_2Y1zGvvX-S_56BeRFg==\tvideo.test.com\thttp\t371\t4.814"));
	}
}
