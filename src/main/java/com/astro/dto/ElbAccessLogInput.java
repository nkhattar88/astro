package com.astro.dto;

import com.astro.utils.ParseUtils;

import java.io.Serializable;

public class ElbAccessLogInput implements Serializable {

	private String timestamp;
	private String elbName;
	private String clientPort;
	private String backendPort;
	private String requestProcessTime;
	private String backendProcessTime;
	private String responseProcessTime;
	private String elbStatusCode;
	private String backendStatusCode;
	private String receivedBytes;
	private String sentBytes;
	private String request;
	private String userAgent;
	private String sslCipher;
	private String sslProtocol;

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getElbName() {
		return elbName;
	}

	public void setElbName(String elbName) {
		this.elbName = elbName;
	}

	public String getClientPort() {
		return clientPort;
	}

	public void setClientPort(String clientPort) {
		this.clientPort = clientPort;
	}

	public String getBackendPort() {
		return backendPort;
	}

	public void setBackendPort(String backendPort) {
		this.backendPort = backendPort;
	}

	public String getRequestProcessTime() {
		return requestProcessTime;
	}

	public void setRequestProcessTime(String requestProcessTime) {
		this.requestProcessTime = requestProcessTime;
	}

	public String getBackendProcessTime() {
		return backendProcessTime;
	}

	public void setBackendProcessTime(String backendProcessTime) {
		this.backendProcessTime = backendProcessTime;
	}

	public String getResponseProcessTime() {
		return responseProcessTime;
	}

	public void setResponseProcessTime(String responseProcessTime) {
		this.responseProcessTime = responseProcessTime;
	}

	public String getElbStatusCode() {
		return elbStatusCode;
	}

	public void setElbStatusCode(String elbStatusCode) {
		this.elbStatusCode = elbStatusCode;
	}

	public String getBackendStatusCode() {
		return backendStatusCode;
	}

	public void setBackendStatusCode(String backendStatusCode) {
		this.backendStatusCode = backendStatusCode;
	}

	public String getReceivedBytes() {
		return receivedBytes;
	}

	public void setReceivedBytes(String receivedBytes) {
		this.receivedBytes = receivedBytes;
	}

	public String getSentBytes() {
		return sentBytes;
	}

	public void setSentBytes(String sentBytes) {
		this.sentBytes = sentBytes;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public String getSslCipher() {
		return sslCipher;
	}

	public void setSslCipher(String sslCipher) {
		this.sslCipher = sslCipher;
	}

	public String getSslProtocol() {
		return sslProtocol;
	}

	public void setSslProtocol(String sslProtocol) {
		this.sslProtocol = sslProtocol;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("com.astro.dto.ElbAccessLogInput{");
		sb.append("timestamp='").append(timestamp).append('\'');
		sb.append(", elbName='").append(elbName).append('\'');
		sb.append(", clientPort='").append(clientPort).append('\'');
		sb.append(", backendPort='").append(backendPort).append('\'');
		sb.append(", requestProcessTime='").append(requestProcessTime).append('\'');
		sb.append(", backendProcessTime='").append(backendProcessTime).append('\'');
		sb.append(", responseProcessTime='").append(responseProcessTime).append('\'');
		sb.append(", elbStatusCode='").append(elbStatusCode).append('\'');
		sb.append(", backendStatusCode='").append(backendStatusCode).append('\'');
		sb.append(", receivedBytes='").append(receivedBytes).append('\'');
		sb.append(", sentBytes='").append(sentBytes).append('\'');
		sb.append(", request='").append(request).append('\'');
		sb.append(", userAgent='").append(userAgent).append('\'');
		sb.append(", sslCipher='").append(sslCipher).append('\'');
		sb.append(", sslProtocol='").append(sslProtocol).append('\'');
		sb.append('}');
		return sb.toString();
	}

	public ElbAccessLogInput map(String logText) {
		//split using spaces except enclosed in double quotes or singe quotes or enclosing brackets
		String regexPattern = "[^\\s\"'\\[]+|\"[^\"]*\"|'[^']*'|\\[[^\\]]*\\]";
		ParseUtils.mapLogTextToTargetObjectFields(logText, this, regexPattern);
		return this;
	}

	public static void main(String[] args) {
		System.out.println(new ElbAccessLogInput().map("2017-06-28T01:09:54.322406Z test-elb 162.158.26.76:14923 10.12.89.248:8080 0.000043 0.010237 0.000015 200 200 158 333 \"POST http://test.com.my:80/api/checkaccountexists HTTP/1.1\" \"-\" - -"));
	}
}
