package com.astro.function;

import com.astro.dto.ElbAccessLogInput;
import com.astro.dto.ServerAccessLogInput;
import com.astro.utils.SparkTaskUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFunction;
import scala.Tuple2;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Elb Access Log Function Definitions for filter, map, etc. functions
 */
public class ElbAccessLogFunctionDef {

	public static final String LOG_DATE_FORMAT = "yyyy-MM-dd";

	public static final PairFunction<ElbAccessLogInput, Date, Long> mapLogToDateCountPairFn = new PairFunction<ElbAccessLogInput, Date, Long>() {
		private DateFormat dateFormat = new SimpleDateFormat(LOG_DATE_FORMAT);

		@Override
		public Tuple2<Date, Long> call(ElbAccessLogInput v1) throws Exception {
			return new Tuple2<>(dateFormat.parse(v1.getTimestamp()), 1L);
		}
	};

	public static final Function<ElbAccessLogInput, Boolean> filterEmptyTimestampLogFn = new Function<ElbAccessLogInput, Boolean>() {
		@Override
		public Boolean call(ElbAccessLogInput v1) throws Exception {
			return StringUtils.isNotBlank(v1.getTimestamp());
		}
	};

	public static final Function<ElbAccessLogInput, Boolean> filterLogWithErrorResponseFn = new Function<ElbAccessLogInput, Boolean>() {
		@Override
		public Boolean call(ElbAccessLogInput v1) throws Exception {
			return StringUtils.isNotBlank(v1.getElbStatusCode())
					&& !SparkTaskUtils.HTTP_STATUS_200.equalsIgnoreCase(v1.getElbStatusCode());
		}
	};

	public static final PairFunction<ElbAccessLogInput, String, Long> mapLogToStatusCountPairFn = new PairFunction<ElbAccessLogInput, String, Long>() {
		@Override
		public Tuple2<String, Long> call(ElbAccessLogInput v1) throws Exception {
			return new Tuple2<>(v1.getElbStatusCode(), 1L);
		}
	};

	public static final Function<ElbAccessLogInput, Boolean> filterEmptyElbStatusLogFn = new Function<ElbAccessLogInput, Boolean>() {
		@Override
		public Boolean call(ElbAccessLogInput v1) throws Exception {
			return StringUtils.isNotBlank(v1.getElbStatusCode());
		}
	};

	public static final Function<String,ElbAccessLogInput> elbAccessLogInputMapFn = new Function<String,ElbAccessLogInput>() {
		@Override
		public ElbAccessLogInput call(String logText) throws Exception {
			return new ElbAccessLogInput().map(logText);
		}
	};
}
