package com.astro.function;

import com.astro.dto.ServerAccessLogInput;
import com.astro.utils.SparkTaskUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFunction;
import scala.Tuple2;

/**
 * Server Access Log Function Definitions for filter, map, etc. functions
 */
public class ServerAccessLogFunctionDef {

	private static final String OPERATION_TYPE_OBJECT = "OBJECT";

	public static final Function<ServerAccessLogInput, String> mapLogToKeyFn = new Function<ServerAccessLogInput, String>() {
		@Override
		public String call(ServerAccessLogInput v1) throws Exception {
			return v1.getKey();
		}
	};

	public static final Function<ServerAccessLogInput, Long> mapLogToScBytesFn = new Function<ServerAccessLogInput, Long>() {
		@Override
		public Long call(ServerAccessLogInput v1) throws Exception {
			return StringUtils.isNotBlank(v1.getBytesSent()) && StringUtils.isNumeric(v1.getBytesSent())
					? Long.valueOf(v1.getBytesSent()) : 0L;
		}
	};

	public static final PairFunction<ServerAccessLogInput, String, Long> mapLogToKeyCountPairFn = new PairFunction<ServerAccessLogInput, String, Long>() {
		@Override
		public Tuple2<String, Long> call(ServerAccessLogInput v1) throws Exception {
			return new Tuple2<>(v1.getKey(), 1L);
		}
	};

	public static final Function<ServerAccessLogInput, Boolean> filterLogWith200StatusFn = new Function<ServerAccessLogInput, Boolean>() {
		@Override
		public Boolean call(ServerAccessLogInput v1) throws Exception {
			return StringUtils.isNotBlank(v1.getHttpStatus())
					&& SparkTaskUtils.HTTP_STATUS_200.equalsIgnoreCase(v1.getHttpStatus());
		}
	};

	public static final Function<ServerAccessLogInput, Boolean> filterLogWithGetMethodFn = new Function<ServerAccessLogInput, Boolean>() {
		@Override
		public Boolean call(ServerAccessLogInput v1) throws Exception {
			return StringUtils.isNotBlank(v1.getOperation())
					&& v1.getOperation().toLowerCase().contains("." + SparkTaskUtils.HTTP_METHOD_GET.toLowerCase());
		}
	};

	public static final Function<ServerAccessLogInput, Boolean> filterLogWithObjectTypeFn = new Function<ServerAccessLogInput, Boolean>() {
		@Override
		public Boolean call(ServerAccessLogInput v1) throws Exception {
			return StringUtils.isNotBlank(v1.getOperation())
					&& v1.getOperation().toLowerCase().endsWith(OPERATION_TYPE_OBJECT.toLowerCase());
		}
	};

	public static final Function<String, ServerAccessLogInput> serverAccessLogInputMapFn = new Function<String, ServerAccessLogInput>() {
		@Override
		public ServerAccessLogInput call(String logText) throws Exception {
			return new ServerAccessLogInput().map(logText);
		}
	};
}
