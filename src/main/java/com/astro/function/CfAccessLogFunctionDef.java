package com.astro.function;

import com.astro.dto.CloudfrontAccessLogInput;
import com.astro.utils.SparkTaskUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFunction;
import scala.Tuple2;

/**
 * Cloudfront Access Log Function Definitions for filter, map, etc. functions
 */
public class CfAccessLogFunctionDef {

	public static final Function<String,CloudfrontAccessLogInput> mapLogTextToCloudFrontInputFn = new Function<String, CloudfrontAccessLogInput>() {
		@Override
		public CloudfrontAccessLogInput call(String logText) throws Exception {
			return new CloudfrontAccessLogInput().map(logText);
		}
	};

	public static final Function<CloudfrontAccessLogInput, String> mapLogToUriStemFn = new Function<CloudfrontAccessLogInput, String>() {
		@Override
		public String call(CloudfrontAccessLogInput v1) throws Exception {
			return v1.getCsUriStem();
		}
	};

	public static final Function<CloudfrontAccessLogInput, Long> mapLogToScBytesFn = new Function<CloudfrontAccessLogInput, Long>() {
		@Override
		public Long call(CloudfrontAccessLogInput v1) throws Exception {
			return StringUtils.isNotBlank(v1.getScBytes()) && StringUtils.isNumeric(v1.getScBytes())
					? Long.valueOf(v1.getScBytes()) : 0L;
		}
	};

	public static final PairFunction<CloudfrontAccessLogInput, String, Long> mapLogToClientIPCountPairFn = new PairFunction<CloudfrontAccessLogInput, String, Long>() {
		@Override
		public Tuple2<String, Long> call(CloudfrontAccessLogInput v1) throws Exception {
			return new Tuple2<>(v1.getClientIP(), 1L);
		}
	};

	public static final PairFunction<CloudfrontAccessLogInput, String, Long> mapLogToEdgeLocationScBytesPairFn = new PairFunction<CloudfrontAccessLogInput, String, Long>() {
		@Override
		public Tuple2<String, Long> call(CloudfrontAccessLogInput v1) throws Exception {
			Long scBytes = StringUtils.isNotBlank(v1.getScBytes()) && StringUtils.isNumeric(v1.getScBytes())
					? Long.valueOf(v1.getScBytes()) : 0L;
			return new Tuple2<>(v1.getEdgeLocation(), scBytes);
		}
	};

	public static final PairFunction<CloudfrontAccessLogInput, String, Long> mapLogToUriStemCountPairFn = new PairFunction<CloudfrontAccessLogInput, String, Long>() {
		@Override
		public Tuple2<String, Long> call(CloudfrontAccessLogInput v1) throws Exception {
			return new Tuple2<>(v1.getCsUriStem(), 1L);
		}
	};

	public static final Function<CloudfrontAccessLogInput, Boolean> filterLogWithPassStatusFn = new Function<CloudfrontAccessLogInput, Boolean>() {
		@Override
		public Boolean call(CloudfrontAccessLogInput v1) throws Exception {
			return StringUtils.isNotBlank(v1.getScStatus())
					&& SparkTaskUtils.HTTP_STATUS_200.equalsIgnoreCase(v1.getScStatus());
		}
	};

	public static final Function<CloudfrontAccessLogInput, Boolean> filterLogWithGetMethodFn = new Function<CloudfrontAccessLogInput, Boolean>() {
		@Override
		public Boolean call(CloudfrontAccessLogInput v1) throws Exception {
			return StringUtils.isNotBlank(v1.getCsMethod())
					&& SparkTaskUtils.HTTP_METHOD_GET.equalsIgnoreCase(v1.getCsMethod());
		}
	};

	public static final Function<CloudfrontAccessLogInput, Boolean> filterEmptyCsUriStemFn = new Function<CloudfrontAccessLogInput, Boolean>() {
		@Override
		public Boolean call(CloudfrontAccessLogInput v1) throws Exception {
			return StringUtils.isNotBlank(v1.getCsUriStem());
		}
	};

	public static final Function<CloudfrontAccessLogInput, Boolean> filterEmptyClientIPFn = new Function<CloudfrontAccessLogInput, Boolean>() {
		@Override
		public Boolean call(CloudfrontAccessLogInput v1) throws Exception {
			return StringUtils.isNotBlank(v1.getClientIP());
		}
	};

	public static final Function<CloudfrontAccessLogInput, Boolean> filterEmptyEdgeLocationFn = new Function<CloudfrontAccessLogInput, Boolean>() {
		@Override
		public Boolean call(CloudfrontAccessLogInput v1) throws Exception {
			return StringUtils.isNotBlank(v1.getEdgeLocation());
		}
	};
}
