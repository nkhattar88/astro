package com.astro.task;

import com.astro.dto.ElbAccessLogAnalysisOutput;
import com.astro.dto.ElbAccessLogInput;
import com.astro.function.ElbAccessLogFunctionDef;
import com.astro.utils.SparkTaskUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Map;

public class ElbAccessLogAnalysisTask extends AbstractSparkTask {

	private static final Logger LOGGER = Logger.getLogger(ElbAccessLogAnalysisTask.class);

	public static void main(String[] args) throws IOException {

		if(args.length < 1) {
			throw new IllegalArgumentException("ElbAccessLog InputPath is required");
		}

		String logInputPath = args[0];
		if(StringUtils.isBlank(logInputPath)) {
			throw new IllegalArgumentException("ElbAccessLog InputPath cannot be blank");
		}

		String analysisOutputPath = "."+ File.separator + "ElbAccessLogAnalysisTask_output.txt";
		if(args.length > 1) {
			analysisOutputPath = args[1];
		}

		ElbAccessLogAnalysisOutput logAnalysisOutput =
				new ElbAccessLogAnalysisTask().execute(logInputPath);

		String ouputText = "***** ELB_ACCESS_LOG ANALYSIS OUTPUT *****" + "\n" + logAnalysisOutput;
		LOGGER.info(ouputText);

		SparkTaskUtils.writeLogAnalysisOutputToFile(ouputText, analysisOutputPath);
	}

	/**
	 * Execute method for Elastic Load Balancer AccessLog Analysis task
	 *
	 * @param inputLogPath	Input Elb AccessLog file Path
	 * @return ElbAccessLogAnalysisOutput
	 */
	public ElbAccessLogAnalysisOutput execute(String inputLogPath) {

		JavaSparkContext sc = initJavaSparkContext(this.getClass().getName());

		JavaRDD<String> inputLogRdd = sc.textFile(inputLogPath);

		JavaRDD<ElbAccessLogInput> cfLogRdd = inputLogRdd
				.filter(SparkTaskUtils.filterEmptyFn)
				.map(ElbAccessLogFunctionDef.elbAccessLogInputMapFn);

		// Total request for each response code
		// filter empty --> mapToPair --> reduceByKey --> collect
		Map<String,Long> totalRequestByCode = cfLogRdd
				.filter(ElbAccessLogFunctionDef.filterEmptyElbStatusLogFn)
				.mapToPair(ElbAccessLogFunctionDef.mapLogToStatusCountPairFn)
				.reduceByKey(SparkTaskUtils._reduceNumericSumFn)
				.collectAsMap();

		// Total error response in a day
		// filter by Error --> filter empty timelogs --> mapToPair --> reduceByKey --> collect
		Map<Date, Long> totalErrorPerDay = cfLogRdd
				.filter(ElbAccessLogFunctionDef.filterLogWithErrorResponseFn)
				.filter(ElbAccessLogFunctionDef.filterEmptyTimestampLogFn)
				.mapToPair(ElbAccessLogFunctionDef.mapLogToDateCountPairFn)
				.reduceByKey(SparkTaskUtils._reduceNumericSumFn)
				.collectAsMap();

		ElbAccessLogAnalysisOutput logAnalysisOutput = new ElbAccessLogAnalysisOutput();
		logAnalysisOutput.setTotalRequestByCode(totalRequestByCode);
		logAnalysisOutput.setTotalErrorPerDay(totalErrorPerDay);

		return logAnalysisOutput;
	}
}
