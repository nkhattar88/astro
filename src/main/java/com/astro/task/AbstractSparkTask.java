package com.astro.task;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;

import java.io.Serializable;

/**
 * Abstract Class for each Spark Task containing common methods
 */
public abstract class AbstractSparkTask implements Serializable {

	protected JavaSparkContext initJavaSparkContext(String appName) {
        SparkConf conf = new SparkConf().setAppName(appName);
        return new JavaSparkContext(conf);
    }
}
