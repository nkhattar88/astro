package com.astro.task;

import com.astro.utils.SparkTaskUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.mapred.lib.MultipleTextOutputFormat;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.PairFunction;
import scala.Tuple2;

import java.io.File;

public class AccessLogAggregationTask extends AbstractSparkTask {

	public static final String CLOUDFRONT_LOG_KEY = "cloudfront";
	public static final String ELB_LOG_KEY = "elb";
	public static final String SERVER_LOG_KEY = "server";
	public static final String LOG_KEY_SEPARATOR = "_";

	public static void main(String[] args) {

		if(args.length < 2) {
			throw new IllegalArgumentException("Both LogInput & LogAggregateOutput paths are required");
		}

		String logInputPath = args[0];
		String aggregateOutputPath = args[1];

		if(StringUtils.isBlank(logInputPath)) {
			throw new IllegalArgumentException("LogInputPath cannot be blank");
		}

		if(StringUtils.isBlank(aggregateOutputPath)) {
			throw new IllegalArgumentException("LogAggregateOutputPath cannot be blank");
		}

		new AccessLogAggregationTask().execute(logInputPath, aggregateOutputPath);
	}

	/**
	 * Execute method for AccessLog Aggregation task
	 *
	 * @param logFilePath Input AccessLog file path
	 * @param aggregateOutputPath Output Log Aggregate Directory Path
	 */
	public void execute(String logFilePath, String aggregateOutputPath) {

		JavaSparkContext sc = initJavaSparkContext(this.getClass().getName());

		JavaRDD<String> inputLogRdd = sc.textFile(logFilePath);

		//map to prfixed-key value pair with filer to remove empty lines
		JavaPairRDD<String, Integer> inputPairRdd =
				inputLogRdd
				.filter(SparkTaskUtils.filterEmptyFn)
				.mapToPair(new KeyPrefixLogPairFunction());

		//save the rdd as text file using custom output format
		inputPairRdd.saveAsHadoopFile(aggregateOutputPath, String.class, Integer.class, KeyPrefixBasedMultiOutputFormat.class);
	}

	/**
	 * KeyPrefixLogPairFunction : Pair Function to convert to pair with respective prefix for every line
	 */
	public static class KeyPrefixLogPairFunction implements PairFunction<String, String, Integer> {

		@Override
		public Tuple2<String, Integer> call(String inputLine) throws Exception {
			String lowercaseInput = inputLine.toLowerCase();
			if (lowercaseInput.contains(CLOUDFRONT_LOG_KEY.toLowerCase())) {
				return new Tuple2<>(CLOUDFRONT_LOG_KEY + LOG_KEY_SEPARATOR + inputLine, 1);
			} else if (lowercaseInput.contains(ELB_LOG_KEY.toLowerCase())) {
				return new Tuple2<>(ELB_LOG_KEY + LOG_KEY_SEPARATOR + inputLine, 1);
			} else {
				return new Tuple2<>(SERVER_LOG_KEY + LOG_KEY_SEPARATOR + inputLine, 1);
			}
		}
	}

	/**
	 * Custom MultipleTextOutputFormat class
	 * first distribute key-value pairs based on their prefix
	 * second remove prefix from key before writing
	 */
	public static class KeyPrefixBasedMultiOutputFormat extends MultipleTextOutputFormat<String, Integer> {

		@Override
		protected Integer generateActualValue(String key, Integer value) {
			return null;
		}

		@Override
		protected String generateActualKey(String key, Integer value) {
			return key.substring(key.indexOf(LOG_KEY_SEPARATOR)+1);
		}

		@Override
		protected String generateFileNameForKeyValue(String key, Integer value, String name) {
			return key.substring(0, key.indexOf(LOG_KEY_SEPARATOR)) + File.separator + name;
		}
	}


}
