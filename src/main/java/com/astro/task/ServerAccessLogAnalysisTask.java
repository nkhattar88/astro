package com.astro.task;

import com.astro.comparator.TupleValueComparator;
import com.astro.dto.ServerAccessLogAnalysisOutput;
import com.astro.dto.ServerAccessLogInput;
import com.astro.function.ServerAccessLogFunctionDef;
import com.astro.utils.SparkTaskUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class ServerAccessLogAnalysisTask extends AbstractSparkTask {

	private static final Logger LOGGER = Logger.getLogger(ServerAccessLogAnalysisTask.class);

	public static void main(String[] args) throws IOException {

		if(args.length < 1) {
			throw new IllegalArgumentException("ServerAccessLog InputPath is required");
		}

		String logInputPath = args[0];
		if(StringUtils.isBlank(logInputPath)) {
			throw new IllegalArgumentException("ServerAccessLog InputPath cannot be blank");
		}

		String analysisOutputPath = "."+ File.separator + "ServerAccessLogAnalysisTask_output.txt";
		if(args.length > 1) {
			analysisOutputPath = args[1];
		}

		int numPopularObjects = 10;
		if(args.length > 2) {
			numPopularObjects = Integer.valueOf(args[2]);
		}

		ServerAccessLogAnalysisOutput logAnalysisOutput =
				new ServerAccessLogAnalysisTask().execute(args[0], numPopularObjects);

		String ouputText = "***** SERVER_ACCESS_LOG ANALYSIS OUTPUT *****" + "\n" + logAnalysisOutput;
		LOGGER.info(ouputText);

		SparkTaskUtils.writeLogAnalysisOutputToFile(ouputText, analysisOutputPath);

	}

	/**
	 * Execute method for Server AccessLog Analysis task
	 * @param inputLogPath	Input Server AccessLog file Path
	 * @param numPopularObjects	Number of Popular Objects to compute
	 * @return ServerAccessLogAnalysisOutput
	 */
	public ServerAccessLogAnalysisOutput execute(String inputLogPath, int numPopularObjects) {

		JavaSparkContext sc = initJavaSparkContext(this.getClass().getName());
		JavaRDD<String> inputLogRdd = sc.textFile(inputLogPath);
		JavaRDD<ServerAccessLogInput> cfLogRdd = inputLogRdd
				.filter(SparkTaskUtils.filterEmptyFn)
				.map(ServerAccessLogFunctionDef.serverAccessLogInputMapFn);

		// Top N Popular Objects
		// filter Object type log --> mapToPair --> reduceByKey --> top
		List<Tuple2<String,Long>> topPopularObjects = cfLogRdd
				.filter(ServerAccessLogFunctionDef.filterLogWithObjectTypeFn)
				.mapToPair(ServerAccessLogFunctionDef.mapLogToKeyCountPairFn)
				.reduceByKey(SparkTaskUtils._reduceNumericSumFn)
				.top(numPopularObjects, new TupleValueComparator<String, Long>());

		// Data downloaded Log
		// NOTE : Here the assumption taken is Downloaded Object
		// equals to HTTP GET Request with 200 Response Status. This may change as per requirement
		// filter Object type log --> filter by GET --> filter by 200 response code
		JavaRDD<ServerAccessLogInput> downloadedObjectLogRdd = cfLogRdd
				.filter(ServerAccessLogFunctionDef.filterLogWithObjectTypeFn)
				.filter(ServerAccessLogFunctionDef.filterLogWithGetMethodFn)
				.filter(ServerAccessLogFunctionDef.filterLogWith200StatusFn);

		// Total amount of data downloaded in bytes
		// map --> reduce
		Long totalDownloadedData = downloadedObjectLogRdd
				.map(ServerAccessLogFunctionDef.mapLogToScBytesFn)
				.reduce(SparkTaskUtils._reduceNumericSumFn);

		// Total distinct object downloaded
		// filter empty object --> map --> distinct --> count
		Long totalDistinctObjectDownloaded = downloadedObjectLogRdd
				.filter(ServerAccessLogFunctionDef.filterLogWithObjectTypeFn)
				.map(ServerAccessLogFunctionDef.mapLogToKeyFn)
				.distinct()
				.count();

		ServerAccessLogAnalysisOutput output = new ServerAccessLogAnalysisOutput();
		output.setTopPopularObjects(topPopularObjects);
		output.setTotalDownloadedData(totalDownloadedData);
		output.setTotalDistinctObjectDownloaded(totalDistinctObjectDownloaded);

		return output;
	}
}
