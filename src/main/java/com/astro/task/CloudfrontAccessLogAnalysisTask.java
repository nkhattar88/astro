package com.astro.task;

import com.astro.comparator.TupleValueComparator;
import com.astro.dto.CfAccessLogAnalysisOutput;
import com.astro.dto.CloudfrontAccessLogInput;
import com.astro.function.CfAccessLogFunctionDef;
import com.astro.utils.SparkTaskUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class CloudfrontAccessLogAnalysisTask extends AbstractSparkTask {

	private static final Logger LOGGER = Logger.getLogger(CloudfrontAccessLogAnalysisTask.class);

	public static void main(String[] args) throws IOException {

		if(args.length < 1) {
			throw new IllegalArgumentException("CloudfrontAccessLog InputPath is required");
		}

		String logInputPath = args[0];
		if(StringUtils.isBlank(logInputPath)) {
			throw new IllegalArgumentException("CloudfrontAccessLog InputPath cannot be blank");
		}

		String analysisOutputPath = "."+ File.separator + "CloudfrontAccessLogAnalysisTask_output.txt";
		if(args.length > 1) {
			analysisOutputPath = args[1];
		}

		int numPopularObjects = 10;
		if(args.length > 2) {
			numPopularObjects = Integer.valueOf(args[2]);
		}

		int numTopClientIP = 10;
		if(args.length > 3) {
			numTopClientIP = Integer.valueOf(args[3]);
		}

		CfAccessLogAnalysisOutput logAnalysisOutput =
				new CloudfrontAccessLogAnalysisTask().execute(logInputPath, numPopularObjects, numTopClientIP);

		String ouputText = "***** CLOUD_FRONT_ACCESS_LOG ANALYSIS OUTPUT *****" + "\n" + logAnalysisOutput;
		LOGGER.info(ouputText);

		SparkTaskUtils.writeLogAnalysisOutputToFile(ouputText, analysisOutputPath);
	}


	/**
	 *
	 * Execute method for Cloudfront AccessLog Analysis task
	 *
	 * @param inputLogPath	Input Cloudfront AccessLog file Path
	 * @param numPopularObjects	Number of Popular Objects to compute
	 * @param numTopClientIP	Number of Top Client Ips to compute
	 * @return CfAccessLogAnalysisOutput
	 */
	public CfAccessLogAnalysisOutput execute(String inputLogPath, int numPopularObjects, int numTopClientIP) {

		JavaSparkContext sc = initJavaSparkContext(this.getClass().getName());

		JavaRDD<String> inputLogRdd = sc.textFile(inputLogPath);

		JavaRDD<CloudfrontAccessLogInput> cfLogRdd = inputLogRdd
				.filter(SparkTaskUtils.filterEmptyFn)
				.map(CfAccessLogFunctionDef.mapLogTextToCloudFrontInputFn);

		// Top N Popular Objects
		// filter empty --> mapToPair --> reduceByKey --> top
		List<Tuple2<String,Long>> topPopularObjects = cfLogRdd
				.filter(CfAccessLogFunctionDef.filterEmptyCsUriStemFn)
				.mapToPair(CfAccessLogFunctionDef.mapLogToUriStemCountPairFn)
				.reduceByKey(SparkTaskUtils._reduceNumericSumFn)
				.top(numPopularObjects, new TupleValueComparator<String, Long>());

		// Data downloaded Log
		// NOTE : Here the assumption taken is Downloaded Object
		// equals to HTTP GET Request with 200 Response Status. This may change as per requirement
		// filter by GET --> filter by 200 response code
		JavaRDD<CloudfrontAccessLogInput> downloadedLogRdd = cfLogRdd
				.filter(CfAccessLogFunctionDef.filterLogWithGetMethodFn)
				.filter(CfAccessLogFunctionDef.filterLogWithPassStatusFn);

		// Total amount of data downloaded in bytes
		// map --> reduce
		Long totalDownloadedData = downloadedLogRdd
				.map(CfAccessLogFunctionDef.mapLogToScBytesFn)
				.reduce(SparkTaskUtils._reduceNumericSumFn);

		// Total distinct object downloaded
		// filter empty object --> map --> distinct --> count
		Long totalDistinctObjectDownloaded = downloadedLogRdd
				.filter(CfAccessLogFunctionDef.filterEmptyCsUriStemFn)
				.map(CfAccessLogFunctionDef.mapLogToUriStemFn)
				.distinct()
				.count();

		// Top n IP repo Client IP
		// filter empty --> mapToPair --> reduceByKey --> top
		List<Tuple2<String, Long>> topClientIPs = cfLogRdd
				.filter(CfAccessLogFunctionDef.filterEmptyClientIPFn)
				.mapToPair(CfAccessLogFunctionDef.mapLogToClientIPCountPairFn)
				.reduceByKey(SparkTaskUtils._reduceNumericSumFn)
				.top(numTopClientIP, new TupleValueComparator<String, Long>());

		// Total data served from each Edge Location
		// filter empty --> mapToPair --> reduceByKey --> collect
		Map<String, Long> totalDataServedPerEdgeLocation = cfLogRdd
				.filter(CfAccessLogFunctionDef.filterEmptyEdgeLocationFn)
				.mapToPair(CfAccessLogFunctionDef.mapLogToEdgeLocationScBytesPairFn)
				.reduceByKey(SparkTaskUtils._reduceNumericSumFn)
				.collectAsMap();

		CfAccessLogAnalysisOutput logAnalysisOutput = new CfAccessLogAnalysisOutput();
		logAnalysisOutput.setTopPopularObjects(topPopularObjects);
		logAnalysisOutput.setTotalDownloadedData(totalDownloadedData);
		logAnalysisOutput.setTotalDistinctObjectDownloaded(totalDistinctObjectDownloaded);
		logAnalysisOutput.setTopClientIPs(topClientIPs);
		logAnalysisOutput.setTotalDataServedPerEdgeLocation(totalDataServedPerEdgeLocation);

		return logAnalysisOutput;

	}

}
